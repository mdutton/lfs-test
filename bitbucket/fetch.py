#!/usr/bin/env python3

import requests
import sys
import hashlib
import json
from requests.auth import HTTPBasicAuth
import yaml

_, url, username, password, hashval, size = sys.argv
size = int(size)

POST_HEADERS={
    "Accept": "application/vnd.git-lfs+json",
    "Content-Type": "application/vnd.git-lfs+json"
}

request_body = {
  "operation": "download",
  "transfers": [ "basic" ],
  "objects": [
    {
      "oid": hashval,
      "size": 0 #int(size)
    }
  ],
  "hash_algo": "sha256"
}

http_res = requests.post(url,json=request_body,headers=POST_HEADERS,auth=HTTPBasicAuth(username, password))
http_res.raise_for_status()

json_res = http_res.json()
download_action = json_res['objects'][0]['actions']['download']
print( yaml.dump(download_action) )

href = download_action['href']
headers = dict(POST_HEADERS)
if 'header' in download_action:
    headers.update( download_action['header'] )

with requests.get(href,headers=headers,stream=True) as download_res:
    print(download_res.status_code)
    download_res.raise_for_status()

    hashfunc = hashlib.sha256()
    fetch_size = 0
    for chunk in download_res.iter_content(None):
        fetch_size += len(chunk)
        hashfunc.update(chunk)
    fetch_hash = hashfunc.hexdigest()

print(fetch_size)
print(fetch_hash)

if hashval == fetch_hash and size == fetch_size:
    print("GOOD")
