#!/usr/bin/env python3

from distutils.command.upload import upload
from email import header
import requests
import sys
import hashlib
import json
from requests.auth import HTTPBasicAuth
import yaml

_, url, username, password, filepath = sys.argv

def read_chunks(filepath,mode="rb",size=-1):
  with open(filepath, mode) as f:
    while True:
      chunk = f.read(size)
      if chunk:
        yield chunk
      else:
        if chunk is not None:
          return

hashfunc = hashlib.sha256()
calcsize = 0
for chunk in read_chunks(filepath,size=4096):
  hashfunc.update(chunk)
  calcsize += len(chunk)
hashval = hashfunc.hexdigest()

POST_HEADERS={
    "Accept": "application/vnd.git-lfs+json",
    "Content-Type": "application/vnd.git-lfs+json"
}

request_body = {
  "operation": "upload",
  "transfers": [ "basic" ],
  "objects": [
    {
      "oid": hashval,
      "size": calcsize
    }
  ],
  "hash_algo": "sha256"
}

print("Request--")
print( yaml.dump(request_body) )

http_res = requests.post(url,json=request_body,headers=POST_HEADERS,auth=HTTPBasicAuth(username, password))
http_res.raise_for_status()

json_res = http_res.json()
print("Response--")
print( yaml.dump(json_res) )

def parse_action(action):
  href = action['href']
  headers = dict(POST_HEADERS)
  if 'header' in action:
    headers.update(action['header'])
  if 'Transfer-Encoding' in headers:
    del headers['Transfer-Encoding']
  return href, headers


def upload_file(upload_entry,filepath):
    if 'actions' in upload_entry:
      if 'upload' in upload_entry['actions']:
        href, headers = parse_action(upload_entry['actions']['upload'])
        print(yaml.dump({
          "href": href,
          "headers": headers
        }))
        with open(filepath,"rb") as f:
          resp = requests.put(href, headers=headers, data=f)
        resp.raise_for_status()
        print("UPLOADED")

      if 'verify' in upload_entry['actions']:
        href, headers = parse_action(upload_entry['actions']['verify'])
        resp = requests.post(href, headers=headers,json=upload_entry,auth=HTTPBasicAuth(username, password))
        resp.raise_for_status()
        print(resp)
        print( yaml.dump(resp.json()) )
        print("VERIFIED")
    else:
      print("Server says the file is already uploaded")

upload_file(json_res['objects'][0],filepath)
